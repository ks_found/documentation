# Aggregators

An **aggregator** is an HTTP server that hosts a database of ZWI files.

## Downloading files from an aggregator
The database is hosted at `<aggregator URL>/encyclosphere/database`. Visiting `<aggregator URL>/encyclosphere/database` in your browser should display an overview of the database.

To download a file, you must first find its location in the database. To find the file you're looking for, refer to the [database section of the documentation](databases.md#structure).

Once you've got the location, here's how you download it. Say the path in the database is `en/handwiki/handwiki.org/wiki#Encyclosphere.zwi`. To download it, make a GET request to `<aggregator URL>/encyclosphere/database/en/handwiki/handwiki.org/wiki#Encyclosphere.zwi`.

## Uploading files to an aggregator

To upload a file to an aggregator, make a `POST` request to `<aggregator URL>/encyclosphere/upload/`. The trailing slash (slash at the end of the URL) is required.

The `Content-Type` must be `multipart/form-data`. One part, named `zwi`, contains the ZWI itself. The other part, named `pass`, contains the password, if required. Example:

```
POST /encyclosphere/upload/ HTTP/1.1
...
Content-Type: multipart/form-data; boundary=---------------------------Boundary

---------------------------Boundary
Content-Disposition: form-data; name="zwi"; filename="Knowledge.zwi"
Content-Type: application/zip

<ZWI contents>

---------------------------Boundary
Content-Disposition: form-data; name="pass"
Content-Type: text/plain

0123456789

---------------------------Boundary
```

#### Response

Successful upload:

```json
{
  "Success": true
}
```

Failed upload:

```json
{
  "Success": false,
  "Reason": "Invalid signature"
}
```

`Success` indicates whether the upload was successful.

`Reason`, only present if the upload failed, indicates why it failed. 
