# The ZWI format

The **ZWI format** is a file format created by the Knowledge Standards Foundation, for the purpose of storing and transmitting encyclopedia articles. ZWI stands for "zipped wiki," and is pronounced "zwee."


## Structure

A ZWI file is actually a ZIP file with a different extension, meaning it has multiple files inside of it. Here's what the contents of a ZWI file looks like:
```
Example_article.zwi
├── data
│   ├── css
│   │   └── main.css
│   └── media
│       ├── images
│       │   └── image.png
│       └── videos
│           └── video.mp4
├── article.html
├── article.txt
├── article.wikitext
├── media.json
├── metadata.json
└── signature.json
```


## Required files

2 files are required: `metadata.json` and `data/colicon.png`. The requirements for these files are described in detail below.


## `metadata.json`

Every ZWI file has a file named `metadata.json` in the root. This is the most important file in the ZWI. It contains essential information about the article, like the title, publisher, language, and source URL. Here's the `metadata.json` from our example article:
```json
{
    "ZWIversion": 1.4,
    "Title": "Example article (example)",
    "ShortTitle": "Example article",
    "Disambiguator": "example",
    "Lang": "en",
    "Content": {
        "article.html": "d5a077655c7b4049404210d473463a33d0d79378",
        "article.wikitext": "99270e7fcdba7eaa9e8868e89810e11da4047eb0",
        "article.txt": "027f9ec9b7a4275d6e3cd1508909bc2b198d086a"
    },
    "Primary": "article.html",
    "Revisions": [],
    "Publisher": "examplepedia",
    "CollectionTitle": "Examplepedia",
    "Authors": [
        "username1",
        "Another_Username"
    ],
    "Editors": [
        "Contributor1",
        "contributor_2"
    ],
    "LastModified": "499138500",
    "TimeCreated": "499138500",
    "Categories": [
        "Example articles"
    ],
    "Topics": [
        "Documentation"
    ],
    "Rating": [],
    "Description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Nullam vehicula ipsum a arcu cursus. Cras sed felis eget velit aliquet sagittis id consectetur. Id porta nibh venenatis cras sed felis eget velit aliquet. Elit pe",
    "ShortDescription": "An example article to explain the ZWI format",
    "Comment": "Additional information goes here",
    "License": "CC BY-SA 3.0",
    "SourceURL": "https://examplepedia.org/wiki/Example_article"
}
```

### Required fields

Field                 | Data type        | Explanation
---                   | ---              | ---
`ZWIversion`          | Number           | The version of the ZWI specification the article adheres to (currently 1.3)
`Title`               | String           | The original title of the article, with no modifications
`Lang`                | String           | The [ISO 639-1](https://www.iso.org/standard/22109.html) 2-letter language code representing the language the article is in ([list of codes](https://www.sitepoint.com/iso-2-letter-language-codes))
`Content`             | Object           | A list of [article files](#article-files) and their SHA-1 hashes
`Publisher`           | String           | The ID of the person or organization that published the article. Publisher IDs must be lowercase with no spaces.
`CollectionTitle`     | String           | The human-readable name of the encyclopedia the article belongs to
`LastModified`        | String           | The last time the ZWI file was modified, in seconds since the Unix epoch
`TimeCreated`         | String           | The time the ZWI file was created, in seconds since the Unix epoch
`Description`         | String           | The first ~350 characters of the article
`License`             | String           | The article's license
`SourceURL`           | String           | The URL of the original source of the article

### Optional fields
Field                 | Data type        | Explanation
---                   | ---              | ---
`Categories`          | Array of strings | A list of categories. No attempt is made to standardize categories.
`Primary`             | String           | The main/preferred article file. If there are no article files, this field should be left blank.
`Authors`             | Array of strings | A list of people that created the article. Can be real names or usernames.
`Editors`             | Array of strings | A list of people that worked on the article
`CreatorNames`        | Array of strings | A list of people that created the article. Can be real names or usernames.
`ContributorNames`    | Array of strings | A list of people that worked on the article
`Topics`              | Array of strings | A list of topics
`ShortDescription`    | String           | A short description of the article
`Comment`             | String           | Additional information about the article itself, not the article's topic
`Rating`              | Array            | Currently unused. In the future, this field will be used to implement a rating system.
`Revisions`           | Array            | Currently unused
`GeneratorName`       | String           | The name of the software that was used to generate the ZWI file
`WordCount`           | Number           | The number of words in the article
`PublicationDate`     | String           | The date the article was originally published, in `yyyy-mm-dd` format
`Namespace`           | String           | The namespace the article is in
`ShortTitle`          | String           | A simplified title, with things like disambiguators and namespaces removed. Inverted titles are also de-inverted.
`Disambiguator`       | String           | Some text describing what differentiates this article from articles with the same title. For example, in "God (sculpture)", the `Disambiguator` is `"sculpture"`.
`AlternativeTitles`   | Array of strings | An array of alternative titles. For example, the `AlternativeTitles` for "Bed; Bedchamber; Bedstead" would be `["Bed", "Bedchamber", "Bedstead"]`.
`InvertedTitle`       | Boolean          | Whether the `Title` of the article is inverted, e.g. "God, Image Of"
`Redirect`            | Boolean          | Whether the article is a redirect
`RedirectURL`         | String           | The URL that the article is redirecting to
`CrossReference`      | Boolean          | Whether the article is cross-referencing another article
`CrossReferenceTitle` | String           | The title of the article this article is cross-referencing
`Stub`                | Boolean          | Whether the article is a stub (under 350 characters)
`IsArticle`           | Boolean          | Whether the article is an actual article


## Article files

These files contain the content of the article. They can be in any format and have any extension, but the filename must be `article`. Common file types include HTML (`.html`), Wikicode (`.wikitext`), and plain text (`.txt`; mainly for indexing purposes).

Correct:
- `article.html`
- `article.txt`

Incorrect:
- `my_article.txt`
- `NewArticle.article`


## The `data` folder

This folder contains assets required by article files. Images go in `data/media/images`, videos go in `data/media/videos`, audio goes in `data/media/audio`, CSS goes in `data/css`, and other files go in `data/other`. Here's the `data` folder from our example article:
```
└── data
   ├── css
   │   └── main.css
   └── media
       ├── images
       │   └── image.png
       └── videos
           └── video.mp4
```
If empty, this folder should be omitted.


## `colicon.png`

A file named `colicon.png` must be present in the `data` folder. This file must be a PNG representing the logo of the encyclopedia the article belongs to. It must be square, meaning its width must be equal to its height. It must be at least 64x64 pixels in size, and at most 200 KB.


## `media.json`

This file contains the names of all of the files in the `data` folder, and their SHA-1 hashes. Here's the `media.json` from our example article:
```json
{
	"data/css/main.css": "d4a02937e87bf586a5981195165e8dbb6fc39e99",
	"data/media/images/image.png": "18cdf3fe481f8a80d31dd63a27b9d21f39cf4f8f",
	"data/media/videos/video.mp4": "0dd6b6fd8f1e42bf57c6198741222d96ad2933ce"
}
```


## Signed ZWI files
To ensure the authenticity of a ZWI file, it can be signed. Signed ZWI files adhere to the [DID:PSQR specification](https://vpsqr.com/). Every signed ZWI file has a file named `signature.json` in the root, with the following contents:
```json
{
    "identityName": "Examplepedia",
    "identityAddress": "USA",
    "kid": "did:psqr:examplepedia.org#publish",
    "updated": "2022-09-19T10:27:09.228673715",
    "alg": "ES384",
    "token": "eyJraWQiOi..."
}
```

Field             | Explanation
---               | ---
`identityName`    | The name of the person or organization signing the ZWI file
`identityAddress` | The address of the person or organization signing the ZWI file. Can be a physical address (not necessarily exact), email address, or URL.
`kid`             | The DID:PSQR key ID
`updated`         | The time the signature was created, in ISO 8601 format (`%Y-%m-%dT%H:%M:%S.%f%z`). Must be UTC.
`alg`             | The key algorithm used to generate the signature. This must be `"ES384"` (ECDSA-P384); other algorithms are not supported.
`token`           | The signature itself, the compact serialization of a JSON Web Token (JWT)


When decoded, the JWT payload looks like this:
```json
{
    "iss": "Examplepedia",
    "address": "USA",
    "iat": "2022-09-19T10:27:09.228673715",
    "media": "5b088791abc92c9c2a3507a6e7052881154aa849",
    "metadata": "6e79d9f60e853e6cfc398c015490527b7c0d2310"
}
```

Field       | Explanation
---         | ---
`iss`       | The name of the person or organization signing the ZWI file. Must be the same as `identityName` in `signature.json`.
`address`   | The address of the person or organization signing the ZWI file. Must be the same as `identityAddress` in `signature.json`.
`iat`       | The time the signature was created, in seconds since the Unix epoch. Although it's in a different format, it must represent the same time as `updated` in `signature.json`.
`metadata`  | The SHA-1 hash of the ZWI file's `metadata.json`
`media`     | The SHA-1 hash of the ZWI file's `media.json`

Since `metadata.json` and `media.json` contain the hashes of all of the files in the ZWI, checking their hashes is sufficient to verify that the ZWI file has not been tampered with.

### Steps to implement ZWI signing in your application

1. Install node.js: https://nodejs.org/en/

2. Install the PSQR CLI: `npm install -g psqr`

3. Create an identity: `psqr identity:create did:psqr:examplepedia.org#publish -n "<name>"`

   Replace `examplepedia.org` with your domain. Replace `<name>` with your name or your organization's name.

   For more information on creating identities, [click here](https://github.com/public-square/psqr#psqr-identitynew-kid).

4. Export your identity: `psqr identity:export did:psqr:examplepedia.org -k publish`

   NOTE: The `#publish` at the end of the URI must be omitted.

5. Host the DID document generated by the above command (`did-psqr-examplepedia.org/identity.json`) at `https://examplepedia.org/.well-known/psqr`, replacing `examplepedia.org` with your domain.

5. Use a JavaScript Object Signing and Encryption (JOSE) library to generate a JSON Web Token (JWT). You must import the private key generated by step 4 (located at `did-psqr-examplepedia.org/publish.private.jwk`), and use that to sign the ZWI. Here are some links to JOSE libraries for popular languages:

   JavaScript: https://github.com/square/js-jose

   Java: https://bitbucket.org/b_c/jose4j/wiki/Home ([Maven link](https://mvnrepository.com/artifact/org.bitbucket.b_c/jose4j))

   Python: https://github.com/mpdavis/python-jose

   PHP: https://github.com/nov/jose-php

   Ruby: https://github.com/potatosalad/ruby-jose

6. Add a `signature.json` file to your ZWI file, using the above examples as a guide.

### Steps to implement ZWI verification in your application

1. Use a JOSE library to verify and decode the `token` in `signature.json`.

2. Ensure that the `kid` in the header of the decoded token matches the `kid` in `signature.json`. If it doesn't, the signature is invalid.

3. Ensure that the following fields in the payload of the decoded token match the fields in `signature.json`:
   This field in the payload | must match this field in `signature.json`
   ---                       | ---
   `iss`                     | `identityName`
   `address`                 | `address`
   `iat`                     | `updated` (format is different, but they must represent the same time)
   If they don't match, the signature is invalid.

4. Compare the hashes in the decoded token to the hashes of `metadata.json` and `media.json` in the ZWI. If they don't match, the signature is invalid.

5. Hash all of the files in the ZWI, and compare them to the hashes in `metadata.json` and `media.json`. If they don't match, the signature is invalid.

6. If all the previous steps succeed, the signature is valid.

For example implementations of signing and verifying in Java, refer to:

https://gitlab.com/ks_found/encycloengine/-/blob/master/src/main/java/org/encyclosphere/encycloengine/api/zwi/ZWIFile.java#L191

https://gitlab.com/ks_found/encycloengine/-/blob/master/src/main/java/org/encyclosphere/encycloengine/api/zwi/ZWIFile.java#L241

If you need help signing or verifying ZWI files, [join the KSF Mattermost](https://mm.encyclosphere.org) and contact \@Henry Sanger or \@Sergei Chekanov.


## Metadata-only ZWI files

Some encyclopedias don't allow distributing of their content. For articles from these encyclopedias, we can generate **metadata-only** ZWI files. These are identical to regular ZWI files, except as the name implies, they only have `metadata.json` (and `signature.json` if they're signed). Even if they only have one file, they're still ZIP files. This is to simplify the format and reduce confusion.

Also, the `metadata.json` of these metadata-only ZWI files has an additional property: `SignificantPhrases`, a list of phrases that appear more often in the article than in normal English. If possible, these significant phrases should be ordered from most significant to least significant. Here's the `metadata.json` of a metadata-only ZWI file:
```json
{
    "ZWIversion": 1.3,
    "Title": "Epistemology",
    "Lang": "en",
    "Revisions": [],
    "Publisher": "sep",
    "Authors": [],
    "Editors": [],
    "LastModified": "1643218065",
    "TimeCreated": "1134518400",
    "Categories": [],
    "Topics": [],
    "Rating": [],
    "Description": "The term \u201cepistemology\u201d comes from the Greek words \u201cepisteme\u201d and \u201clogos\u201d. \u201cEpisteme\u201d can be translated as \u201cknowledge\u201d or \u201cunderstanding\u201d or \u201cacquaintance\u201d, while \u201clogos\u201d can be translated as \u201caccount\u201d or \u201cargument\u201d or \u201creason\u201d. Just as each of these different translations captures some facet of the meaning of these Greek terms, so too does each translation capture a different facet of epistemolog",
    "ShortDescription": "",
    "Comment": "",
    "SignificantPhrases": [
        "conee",
        "deontic logic",
        "explanation",
        "apparent fossils",
        "modern science",
        "culturally",
        "circularity dependence coherentism",
        "devitt",
        "successfully",
        "high objective probability",
        "here\u2019s",
        "circular",
        "avoidance",
        "hearer",
        "propositional content",
        "bonjour",
        "moore",
        "replies",
        "historically",
        "object",
        "poorly",
        "constitutivist",
        ...
    ],
    "License": "https://plato.stanford.edu/info.html#terms",
    "SourceURL": "https://plato.stanford.edu/entries/epistemology/"
}
```
