# WordPress Plugin

https://gitlab.com/ks_found/encycloshare

## Description
The 100% free (OSS) EncycloShare plugin adds the encyclopedic articles on your blog to the Encyclosphere. This means submitting your articles to various aggregators, so that your article(s) will (if accepted) appear on [EncycloSearch](https://encyclosearch.org/), [EncycloReader](https://encycloreader.org/), [DARA](https://encyclopedia.dara.global/#), the [EncycloSphere Chrome plugin](https://chrome.google.com/webstore/detail/encyclosphere/olhghbhocpjleboiiigeljhimfilkfnn), and soon others. This is a rapidly growing open, shared, and censorship-resistant database of publicly-available encyclopedia articles, and guarantees that your article will live on long after you are gone.

Contribute to the global encyclopedia network, while retaining total control and licensing over your work, and bypassing Wikipedia.

**Important:** this plugin will not work without setup. Follow the instructions in the **Installation** section.

The plugin provides:
1. a "Publish to Encyclosphere" button with per-article settings, located at **the bottom** of each blog post and page;
2. a Settings page, with global settings; and
3. a Dashboard panel, listing of all encyclopedia posts (ZWI files) that you submit.

The plugin (and its required digital identity plugin) does the following for you, _after the required setup_:
1. allows you to submit articles to Encyclosphere aggregators (see above), including all your text, images, and probably other data as well, preserving your basic formatting;
2. for each submitted article, creates a ZWI file ("zipped wiki": the standard encyclopedia article file format), deposited in a directory that you can make public if you like _**DOUBLECHECK THIS**_;
3. creates a digital identity associated with your blog (which you can use for many purposes), which follows the _entirely self-owned_ digital identity standard DID:PSQR (see below);
4. digitally signs your article, using your DID:PSQR digital identity;
5. allows you to configure basic data about your encyclopedia collection (such as license and language); and
6. allows you to tag your submitted ZWI files with categories, by tying in with the WordPress category system.

NOTE: this **requires** the [Virtual Public Square (VPS) plugin](https://wordpress.org/plugins/virtual-public-square/). **Install the 100% free-and-OSS VPS plugin before attempting to use EncycloShare!**

### In more detail

**The standard file format** for encyclopedia articles, the [ZWI file format](https://docs.encyclosphere.org/#/zwi-format), has many requirements (specified by the [Knowledge Standards Foundation](https://encyclosphere.org/)). The EncycloShare plugin makes your posts fit these requirements. The plugin takes your encyclopedia post, uses it to prepare all necessary files (such as images) and directories following the ZWI standard, and finally compresses the result in a single ZWI file. A ZWI file is a a kind of ZIP file, so you can use any ZIP reader to view the contents, if you like.

**The plugin then posts the ZWI file to an aggregator** or aggregators of your choosing, when you instruct it to do so. Depending on your license, the ZWI file might then (eventually) propagate to other aggregators. Each aggregator is responsible for maintaining its own database. Because Encyclosphere aggregators are developing a shared database standard as well, they can easily share articles, including yours. (We cannot guarantee that your work will be accepted by any given aggregator. Developers can set up their own aggregators, by installing the KSF's free aggregator engine, [EncycloEngine](https://gitlab.com/ks_found/encycloengine).)

**All ZWI files are stored** in a `/zwifiles` subdirectory of the standard WordPress `/uploads` directory. So an article might be found, for example, at `https://YOURDOMAIN.COM/wp-content/uploads/zwifiles/YOUR-ARTICLE-12345.zwi`.

**Publishing**, or submission to the Encyclosphere, happens when you press the "Publish to Encyclosphere" button at the bottom of every WordPress post and page. You cannot, at this time, "unsubmit" an article, so make sure you really want to submit it. If you want to make edits and resubmit, you can. Probably a good way to _delete_ an article from the encyclosphere would be to simply make your page blank, and then submit the blank page. That is sure to be rejected, and the rejected ZWI will probably overwrite the old article, depending on circumstances.

**The Dashboard Panel** shows any ZWI files that have been created. For now, you will have to follow the links provided in the panel to determine whether your submission has been accepted. You can also follow links to download, edit, and view articles in your collection.

### About Digital Signatures

Virtual Public Square (VPSQR) plugin: https://wordpress.org/plugins/virtual-public-square/

**Your ZWI files are digitally signed** and therefore can be authenticated as yours, wherever they eventually can be found on the internet. These digital signatures follow the [did:psqr](https://vpsqr.com/) "Virtual Public Square" digital identity standard. The KSF chose this standard because it allows you to declare and maintain your own identity on your own domain—so it is robustly self-owned. A digital signature is essential to maintaining your credit and claim to ownership in a wide-open network like the Encyclosphere. Aggregators will cryptographically validate your ZWI submissions (EncycloShare handles this for you automatically); such validation is required for acceptance by these network aggregators. All this is why you must install the (equally 100% free) Virtual Public Square plugin to utilize EncycloShare.

**Your digital identity takes the form of a Decentalized Identifier (DID) file.** DID files contain identifying information you wish to share with the public and public keys used to verify your content. Your DID, when it is created, will be accessible at `https://YOURDOMAIN.COM/.well-known/psqr`. The Virtual Public Square plugin uses the W3C's DID standard, and particularly the `did:psqr` specification, to attach proof of provenance to your content for distribution; further information on this specification is available at `https://vpsqr.com`.

**Users are required to generate or upload their keys** before they can publish.
The Virtual Public Square plugin automatically creates, uploads, and stores the relevant identity files according to the DID:PSQR digital identity standards. Instructions on how to do this (it is not hard) can be found under "Installation."


## Installation

**STEP ONE: Install Virtual Public Square plugin.**
First we (a) install the plugin Virtual Public Square and (b) set up the DID:PSQR identity keys from the users list display. Here is how.

WORDPRESS: Admins will see buttons to allow generation, upload or delete signature files for each user.
  
  COMMANDLINE: follow the directions to create a specialized DID:PSQR identity
  https://wordpress.org/plugins/virtual-public-square/
  
  ![image info](img/screenshot-1.png)
  ![image info](img/screenshot-2.png)
  
 
**STEP TWO: Configure EncycloShare plugin.**
Install the plugin EncycloShare plugin (this one) just like all the others for WordPress. 

Configure your options in Administrative Settings->EncycloShare.

  ![image info](img/screenshot-3.png)


## Usage
Load a post or a page in edit mode and locate the add-in widget for the EncycloShare.
You can alter a few values for each article you publish in ZWI format, these features and additions will expand.
Categories attached to an article are added to the ZWI file format.
Push the 'Publish to Encyclosphere' button to publish them to your selected aggregators.

Publish button is visible for: editor, admin, author, contributor users.
A toggle hidden response returns the results of your publication, responses are NOT saved.

Go To https://encycloreader.org  or https://encyclosearch.org/ to see your articles published


![image info](img/screenshot-4.png)

A Dashboard Panel will list all zwi files found in the uploads directory, created from publishing to aggregators.

    ```
	DISCLAIMER:
		Article publishing is NOT tracked
		Results of publishing is NOT tracked
		Articles can NOT be removed/unpublished with this plugin
	```

## Project
    File Storage: all zwidir/media content MUST be in the WP_UPLOAD directories and ATTACHED to an article.
	This limits issues and bad actors when no control over where files could be coming from.

    Publish button 'Publish to Encyclosphere' is visible for: editor, admin, author, contributor users
    https://wordpress.org/support/article/roles-and-capabilities/#contributor

	
## Structure
A ZWI object is used to create the zwi files for upload to various aggregators.
```

Example ZWIArticle Object
├── [ZWIversion:ZWIArticle:private] => 0.1
├── [GeneratorName:ZWIArticle:private] => wordpress
├── [Primary:ZWIArticle:private] => article.html
├── [pushURLAPI:ZWIArticle:private] => /upload/put.php
├── [Title] => (article title)
├── [Description] => (350 chars of article) 
├── [LastModified] => 2022-09-23 17:49:03
├── [TimeCreated] => 2022-08-09 17:13:40
├── [SourceURL] => https://[DOMAIN]/?page_id=18
├── [CreatorNames] => Array( [0] => username )
├── [ContributorNames] => Array ( [0] => username )
├── [Rating] => Array ( )
├── [Topics] => Array ( )
├── [Categories] => Array ( )
├── [Revisions] => Array( )
├── [ShortDescription] => Try Another Sample Post
├── [Comment:ZWIArticle:private] => 2022-09-23 20:15:46 Generated BY wordpress for zwi version (n)
├── Signature
│   ├── [identityName] => BLOGNAME
│   ├── [identityAddress] => BLOGURL
│   ├── [psqrKid] => did:psqr:[DOMAIN]/author/[USER]#publish
│   ├── [identityName] => BLOGNAME
│   └── [updated] => 2022-09-23T20:15:46.000000
├── Content
│   ├── [articlehtml] => (the article with HTML)
│   └── [articletext] => (the article without HTML)
├── Attachments
│   └── [0]
│   	├── [srcname] => 2022/08/My-file.txt
│   	└── [srctype] => text/plain
├── PlugSettings
│   ├── [zwiaggs_uri] => https://encyclosearch.org/api/aggregators
│   └── zwiaggs_sel
│       ├── [0] => https://encycloreader.org
│   ├── [zwi_directory] => /zwifiles/
│   ├── [zwiset_license] => CC BY-SA 3.0
│   ├── [zwiset_pubcode] => sandbox
│   ├── [zwiset_lang] => en
│   └── [zwiset_signmethod] => ES384
└── [UploadDir:ZWIArticle:private] => /wp-content/uploads/


```

## Workflow
![image info](img/workflow.png)
