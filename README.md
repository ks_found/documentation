# Introduction

**Note:** This documentation is a work in progress.

Imagine all the encyclopedias in the world, connected into one decentralized network, the way all the blogs are loosely connected. You've heard of the blogosphere; now, we’re building an **encyclosphere**. This documentation explains how the current iteration of the Encyclosphere works, enabling YOU to build apps using the standards we've created. For more, less technical information about the Encyclosphere, go to [encyclosphere.org](https://encyclosphere.org).
