# Databases

A **database** is a collection of ZWI files, usually hosted by an [aggregator](aggregators.md).

## Structure
Here's what the file tree of a typical ZWI database looks like:
<pre><code>database/
└── <b>en <————</b>
    ├── examplepedia
    │   ├── en.examplepedia.org
    │   │   └── wiki#Example_article.zwi
    │   └── en.examplepedia.org.csv.gz
    ├── trash
    │   └── examplepedia
    │       ├── en.examplepedia.org
    │       │   └── wiki#Deleted_article.zwi
    │       └── en.examplepedia.org.csv.gz
    └── index.csv.gz
</code></pre>

There's an `en` folder in the top level of this database. `en` is the 2-letter ISO language code for English. [(Here's a handy list of language codes.)](https://www.sitepoint.com/iso-2-letter-language-codes) In the top level of the database, there is a folder for each 2-letter language code, containing all of the articles in the corresponding language.
<br><br><br>



<pre><code>database/
└── en
    ├── <b>examplepedia <————</b>
    │   ├── en.examplepedia.org
    │   │   └── wiki#Example_article.zwi
    │   └── en.examplepedia.org.csv.gz
    ├── trash
    │   └── examplepedia
    │       ├── en.examplepedia.org
    │       │   └── wiki#Deleted_article.zwi
    │       └── en.examplepedia.org.csv.gz
    └── index.csv.gz
</code></pre>
In each language folder (e.g. `en`), there is a folder for each publisher, containing the articles from that publisher.
<br><br><br>



<pre><code>database/
└── en
    ├── examplepedia
    │   ├── en.examplepedia.org
    │   │   └── wiki#Example_article.zwi
    │   └── en.examplepedia.org.csv.gz
    ├── <b>trash <————</b>
    │   └── examplepedia
    │       ├── en.examplepedia.org
    │       │   └── wiki#Deleted_article.zwi
    │       └── en.examplepedia.org.csv.gz
    └── index.csv.gz
</code></pre>
Also in the language folder, there's a special `trash` folder, containing ZWI files marked for deletion. The `trash` folder has the same structure as the parent language folder.
<br><br><br>



<pre><code>database/
└── en
    ├── examplepedia
    │   ├── <b>en.examplepedia.org <————</b>
    │   │   └── wiki#Example_article.zwi
    │   └── en.examplepedia.org.csv.gz
    ├── trash
    │   └── examplepedia
    │       ├── en.examplepedia.org
    │       │   └── wiki#Deleted_article.zwi
    │       └── en.examplepedia.org.csv.gz
    └── index.csv.gz
</code></pre>
In each publisher folder (e.g. `examplepedia`), there is a folder for each domain used by the publisher, containing the articles hosted on that domain. In this example, the only domain is `en.examplepedia.org`.
<br><br><br>



<pre><code>database/
└── en
    ├── examplepedia
    │   ├── en.examplepedia.org
    │   │   └── <b>wiki#Example_article.zwi <————</b>
    │   └── en.examplepedia.org.csv.gz
    ├── trash
    │   └── examplepedia
    │       ├── en.examplepedia.org
    │       │   └── wiki#Deleted_article.zwi
    │       └── en.examplepedia.org.csv.gz
    └── index.csv.gz
</code></pre>
In the domain folders (e.g. `en.examplepedia.org`) are the ZWI files themselves.

The names of the ZWI files start with the part of the `SourceURL` after the domain, with the slashes replaced by pound signs (`#`). (`SourceURL` is a field in the ZWI file's `metadata.json`.)

Any existing pound signs in the URL are escaped with another pound sign: `##`

Finally, `.zwi` is added.

Once again, here's the process to convert a URL to a ZWI filename:

1. Separate path from domain:
```
https://en.examplepedia.org/wiki/Example_article
                            └─────────┬─────────┘
                            This is the part we need
```
```
wiki/Example_article
```
<br>

2. Replace slashes with pound signs:
```
wiki/Example_article ➜ wiki#Example_article
```
<br>

3. Add `.zwi`:
```
wiki#Example_article ➜ wiki#Example_article.zwi
```
<br>


To clarify, here's the `SourceURL` of our example article:
```
https://en.examplepedia.org/wiki/Example_article
```
<br>

And here's the full path of the article in the database:
```
en/examplepedia/en.examplepedia.org/wiki#Example_article.zwi
```
<br>


## Index files
To make lookups easier and faster, databases have **index files**. There are 2 types of index files: meta-index files and ZWI index files.

### Meta-index files
<pre><code>database/
└── en
    ├── examplepedia
    │   ├── en.examplepedia.org
    │   │   └── wiki#Example_article.zwi
    │   └── en.examplepedia.org.csv.gz
    ├── trash
    │   └── examplepedia
    │       ├── en.examplepedia.org
    │       │   └── wiki#Deleted_article.zwi
    │       └── en.examplepedia.org.csv.gz
    └── <b>index.csv.gz <————</b>
</code></pre>

In each language folder (e.g. `en`), there's an `index.csv.gz` file. As the name implies, it's a gzipped CSV file. However, unlike most CSV files, it uses a pipe character as the separator. It's a meta-index file—each row represents a domain folder. Here's what that looks like:
```
timestamp|size|articleCount|path
1672706207|38007|1|examplepedia/en.examplepedia.org
```

The header is for illustrative purposes only. Most real index files won't have a header.

Field | Explanation
--- | ---
`timestamp` | The last time the folder was modified, in seconds since the Unix epoch
`size` | The size of the folder, in bytes
`articleCount` | The number of articles in the folder
`path` | The path to the folder

The field to the left of the separator, `timestamp`, is the last time the index file was modified, in seconds since the Unix epoch.

The field to the right, `indexPath`, is the path to the index file in the database. To get the path, just add the language code (from the parent folder) and a slash to the beginning, and `.csv.gz` to the end. For example:
<pre><code>examplepedia/en.examplepedia.org ➜ <b>en/</b>examplepedia/en.examplepedia.org<b>.csv.gz</b>
</pre></code>

### ZWI index files
<pre><code>database/
└── en
    ├── examplepedia
    │   ├── en.examplepedia.org
    │   │   └── wiki#Example_article.zwi
    │   └── <b>en.examplepedia.org.csv.gz <————</b>
    ├── trash
    │   └── examplepedia
    │       ├── en.examplepedia.org
    │       │   └── wiki#Deleted_article.zwi
    │       └── en.examplepedia.org.csv.gz
    └── index.csv.gz
</code></pre>

In each publisher folder (e.g. `examplepedia`), there's an index file for each domain (e.g. `en.examplepedia.org`). The filename is the domain + `.csv.gz` (e.g. `en.examplepedia.org.csv.gz`). This is a ZWI index file. Just like a meta-index file, it's a gzipped pipe-delimited CSV file. Each row represents a ZWI file in the domain folder. Here's an example:

```
timestamp|filesize|filename
1667602588|38007|wiki#Example_article
```

Again, the header is for illustrative purposes only. Most real index files won't have it.

Field | Explanation
--- | ---
`timestamp` | The last time the article was modified, in seconds since the Unix epoch (`LastModified` in `metadata.json`)
`filesize` | The size of the article, in bytes
`filename` | The filename of the article, with `.zwi` omitted
